local datetime = require "datetime"
local nmap = require "nmap"
local shortport = require "shortport"
local sslcert = require "sslcert"
local stdnse = require "stdnse"
local string = require "string"
local table = require "table"
local tls = require "tls"
local unicode = require "unicode"


author = "J. Erustes & D. Ventas"

categories = {"default","discovery","vuln"} --triggered with -se option

license = "Same as Nmap--See https://nmap.org/book/man-legal.html"

dependencies = {"ssl-cert"}

--
-- Reference information:
--   ssl_cert Lua script
--   Lua 5.3 Documentation
--
-- USAGE:
-- @usage
-- nmap -p 4433 domain.url --script script-name.nse --script-args [...],[...]
--
-- _> Against local server
--
-- nmap --script distrustedTLScerts.nse --script-args='list=list.csv' -v -d1 -p ___ localhost
--
-- _> Against external vulnerable server:
--
-- nmap —script=‘distrustedTLScerts.nse’ —script-args=“list=list.csv” -v -d1 -p 443 hackthis.co.uk
--
-- @args list : csv file containing distrusted domain names to check against (default: 'list.csv')
--
-- ____________________________________________________________
--
-- SERVER & CERTIFICATE:
--
-- 1. Change openssl.cnf to Alternative Names and not rely on CA
--    Locate following sections:
--
--      distinguished_name  = req_distinguished_name
--      attributes    = req_attributes
--      x509_extensions = req_ext
--
--        [ req_distinguished_name ]
--            # fill with data to be used for the certificate
--
--        [ req_ext ]
--        subjectAltName = @alt_names

--        [ alt_names ]
--          DNS.1 = cyberattacks.com
--          DNS.2 = CAtechniques.com
--          DNS.3 = cyberAT.com
--          DNS.4 = CattackT.com
--
-- 2. Create RSA key & Certificate following the openssl.cnf configuration file
-- $ openssl req -x509 -newkey rsa:2048 -keyout key1.pem -out cert1.pem -days 365 -nodes -config openssl.cnf
--
-- 3. Show certificate on screen in a legible way
-- $ openssl req -in cert1.pem -text -noout
--
-- 4. Launch Server with valid cert1 Certificate (signed and with AltNames)
-- $ openssl s_server -key key1.pem -cert cert1.pem -www
--
-- -------------------
--
-- -> Generation of certificate and signature [DISENCOURAGED: Not totally valid]
--
--   Generate new key and certificate
-- $ openssl req -x509 -newkey rsa:2048 -keyout key1.pem -out cert1.pem -days 365 -nodes
--
--   Show certificate on screen in a legible way
-- $ openssl req -in cert1.pem -text -noout
--
--   Levantar HTTPS server on port 44300 with the certificate shown previously
-- $ openssl s_server -key key1.pem -cert cert1.pem -accept 44330 -www
--
--   Check it is correctly running, in browser
-- https://localhost:44330
--
--   Generate cerficate via command line with subjectAltName using existing key:
--          WARNING: this command creates REQUESTS with AltNames, not Certificates
--
-- $ openssl req -new -sha256 -key key1.pem -subj "/C=ES/ST=ES/O=Acme, Inc./CN=cyberattack.com"
--   -reqexts SAN -config <(cat /etc/ssl/openssl.cnf <(printf
--   "[SAN]\nsubjectAltName=DNS:cyberattack.com,DNS:www.cberattack.com,
--   DNS:sistemasciberataque.es,DNS:CAtechniques.ru")) -out cert2.pem
--
--
-- -> Alternative: Having a valid CA to sign        (Not tested)
-- $ openssl ca -cert cert1.pem -keyfile key1.pem -in cert2.pem -out servercrt.pem
--
--  ____________________________________________________________
--
--
-- __> Lauch of openssl Server:
--
-- $ cd ~/<working_directory>/certificates/
--      contains "key_.pem" key file, "cert_.pem" certificate file
--
-- $ openssl s_server -key key1.pem -cert cert1.pem -www
--
--
-- __> Launch of ncat Server (using openssl certificate & key)
--
-- $ ncat -p <___> -c 'read a; echo "HTTP/1.0 200 OK\r\nDate: 
--      $(date)\r\nServer: my ncat server\r\nContent-Type: text/html; 
--      charset=utf-8\r\n\r\n";cat index.html' -k -l --ssl --ssl-key key1.pem 
--      --ssl-cert cert1.pem
--
--
-- __> Check server is UP
--
-- $ netstat -atn | grep <___>
--        port number can vary depending on the receiving server
--          openssl: 4433
--          ncat: 443
--
--
--  ----- DEBUGGING module: check if file is openable -----
--
--    local f = io.open(file, "rb")
--    if (not f) then stdnse.debug1("output_check: ERROR WITH FILE")
--    else
--      stdnse.debug1("function_name: FILE [ OK ]")
--      f:close()
--    end
--
----- ----- ----- ----- ----- ----- -----
--
--
-- https://nmap.org/nsedoc/lib/nmap.html#get_ssl_certificate
--
-- https://www.lua.org/manual/5.3/
--
--



-- RULE SECTION --
portrule = function(host, port)
  return shortport.ssl(host, port) or sslcert.isPortSupported(port) or sslcert.getPrepareTLSWithoutReconnect(port)
end
  --triggered when relevant port is marked as 'open'



-- ACTION SECTION --
  --runs when the script rule triggers


--  --  AUXILIARY FUNCTIONS  --  --

-- See if the supicious-list file exists
function file_exists(file)
    -- file (param) != file (local variable)
    local file = stdnse.get_script_args('list')

    if file == nil then
      file = "list.csv"
    end

    local f = io.open(file, "r")
    if f then f:close() end

    return f ~= nil
end


-- NOT USED in current version
function retrieve_lines(file, altname)
  -- file = io.open(filename)
  --must be called before this function
  local exists;
  local lines = {}

  if not file_exists(file) then return {} end

  file:read()
  for line in io.lines(file) do
    -- lookup: alternative name/OU in the whole line
	  --! only for exact match of the string
    lines[#lines + 1] = line
	  exists = string.find(file, altname)
    if (exists ~= nil) then
      break
    end
  end
  --It can also just 'return exists' and then process later what to do with it
    -- no matches found
  if exists == nil then return {} end
  	--file exists
    -- ret integer: position returned by find()
  return exists
  --return file
	-- we could aslso return full string of blacklist containing altname
end


-- Translate lines from a file (csv in this case)
--  to a local table: easier management, less memory consumption
local function lines_form(file)
  if (not file_exists(file)) then return {} end

  lines = {}
  for line in io.lines(file) do
    --write each line into one entry of the table
    lines[#lines + 1] = line
  end

  return lines
end


-- Split a string according to the 'separation' character
local function split(splitStr, separation)
  local table1 = {}
  if separation == nil then
    separation = "%s"
  end

  local reg = string.format("([^%s]+)", separation)
  for mem in string.gmatch(splitStr, reg) do
    table.insert(table1, mem)
  end

  return table1
end


-- Wildcard processing for altnames & suspected names' list entries
--  args: 'raw' domain name, single string
--  Used in process_name() just below (AltNames) 
--    and is_distrusted()(list entries)
local function process_wildcards(raw)
  local processed = string.gsub(raw, "*", ".*")
  return processed
end


-- process altname to fit into desired format
  -- usually comes as 'DNS: altname.com, DNS:www.altname.com'
  -- wildcard processing
local function process_name(raw)
  local line1 
  local line2
  local altn = {}
  local altn2 = {} 

  -- Names with 'DNS: domain_name.com, DNS: www.domain_name.com' format
  -- Separate 'DNS: domain1' | 'DNS: domain2'
  if (string.find(raw, ", ") ~= nil) then
    line1 = split(raw, ", ")
    altn = line1 -- Split into list{altnames}

      for i in ipairs(altn) do
        stdnse.debug1("process_name[Phase1] = %s", altn[i])
      end
  else -- There is only one AltName, so it returns the 'raw' string
    altn[0] = raw 
  end 

  -- Names with 'DNS: domain_name.com' format
  -- Separate "DNS" | "domain_name.com"
  for j in ipairs(altn) do
    if (string.find(altn[j], ":") ~= nil) then
      line2 = split(altn[j], ":")
      altn2[j] = line2[2] --take second part, that including domain_name
        stdnse.debug1("process_name[Phase2] = %s", altn2[j])
    else
      altn2[j] = altn[j] 
    end
  end

  -- manage wildcards in Cert's AltName 
  -- Substitute * by .* for Lua matching
  for k in ipairs(altn2) do
      stdnse.debug1("Testing wildcards for: %s", altn2[k])
    altn2[k] = process_wildcards(altn2[k])
      stdnse.debug1("process_name[Phase3] = %s", altn2[k])
  end

  return altn2
end


-- Retrieve extension from a certificate
--  extension: extension name in a string
--    Used in check_KeyID()
local function get_extension(cert, extension)
  -- Only when the certificate contains extensions
  if (cert.extensions) then
    for _, e in ipairs(cert.extensions) do
      if (extension == e.name) then
        stdnse.debug1("Extension FOUND: %s", e.name)
        return e
      end
    end
  else return nil
  end
end


-- Source: ssl_cert.nse
-- Convert a date into timestamp format (string)
--    Used in: check_validity(cert, date) [Enhanced Functionality]
local function date_to_string(date)
  if not date then
    return "MISSING"
  end
  if type(date) == "string" then
    return string.format("Can't parse; string is \"%s\"", date)
  else
    return stdnse.format_timestamp(date)
  end
end


-- print table:
-- for i in pairs(table) do
--   print(table[i])
-- end
-- stdnse.debug1("___ table : COMPLETE ")

--_________________________________________________--
-----------------------------------------------------



-- Create local table in which to copy csv file data for more efficient processing.
-- Splits by ';' the file fields
-- @arg: csv file containing suspicious/malicious domains
local function distrusted_table(file)
        -- TODO: next chunk not completely working, unused for now
  -- distTable = {}
  -- for line in io.lines(file) do
  --   l = line .. ";a"  -- TODO: Ñapa: match() ignores last word, I add dummy one
  --   local certainity, severity, name, tmp = l:match("%s*(.-);%s*(.-);%s*(.-);%s*(.-);")
  --   local cmpname = string.gsub(name, "*", ".*")
  --   distTable[#distTable + 1] = {certainity = certainity, severity = severity, name = name, cmpname = cmpname}
  -- end

  local fullTable = lines_form(file)
  local altnTable = {}
  local processedT

  -- Split fullTable (containing full lines)
  --    to leave only the AltNames
  for k in pairs(fullTable) do
    local lineTable = split(fullTable[k], ";")
    altnTable[k] = lineTable[3]
  end

  stdnse.debug1("CREATED DISTRUSTED TABLE  [ OK ] ")
  return altnTable
end


-- Check if 'altname' strings contained in list of domains.
-- Traverses distrusted table checking for matches with the string;
--  if found, saves the match in a table it returns.
-- @args. 'altnames': list of strings with certificates' Alternative Names
--        'distTable': table containing distrusted domains
local function is_distrusted(altnames, distTable)
  local trusted = true
  local matches = {}

      -- Debugging
    for a in ipairs(altnames) do  
      stdnse.debug1("altnames: %s", altnames[a])
    end
      -- stdnse.debug1("is_distrusted(): TRAVERSING distTable: \n")
          -- for i in pairs(distTable) do
          --   print(distTable[i])
          -- end
          -- stdnse.debug1("___ table : COMPLETE ")

  for i in ipairs(altnames) do -- for each of the altnames in the certificate

    for k, t in pairs(distTable) do
      processedT = process_wildcards(t) -- * -> .*

      if altnames[i] == string.match(processedT, altnames[i]) then
        stdnse.debug1("is_distrusted(): MATCH FOUND! Saving to table")

        matches[#matches + 1] = altnames[i]
        -- ALTERNATIVE:       = t 
        --    to show the list entry counted as suspicious
        --    TODO: if this is chosen, 't' must be cleant up from '\x0D' at its tail

          --if the altname is found in the blacklist, certificate is not trusted
        trusted = false
      end
    end
  end
  -- table containing matches of the list. If ==nil, no matches found
  stdnse.debug1("CHECKED MATCHES [ OK ]")
  return matches --, trusted
end


--_________________________________________________--
-----------------------------------------------------


--        ______________________
-- ------ ENHANCED FUNCTIONALITY ------ --
--        ______________________

-- #1. Output a warning if the certificate includes a Subject Key Identifier
--   longer than 20 bytes (40 hex digits).
local function check_KeyID(cert)

  local warning = false
  local ski = get_extension(cert, "X509v3 Subject Key Identifier")
    -- returns the whole extension: name & value
  local skiLen


  if (ski ~= nil) then  
      stdnse.debug1("Subj. Key Identifier: %s", ski.value)

      -- Compute extension's length: groupings separated by :
    skiLen = #(split(ski.value, ':'))
    stdnse.debug1("SKI Lenght = %d", skiLen)

      -- 20 is measuring Bytes
    if (skiLen > 20) then
      -- stdnse.debug1("*** WARNING ***: SKeyIdentifier bigger than 20B, too long!")
      stdnse.debug1("*** WARNING ***: SKeyIdentifier bigger than 20B, too long! \n")
      warning = true
      -- the S_Key_ID is too large
    else
      stdnse.debug1("Key Identifier in check.")
      warning = false
      -- Everything in order
    end
  else 
    stdnse.debug1("Internal warning: Contains no SubjectKeyID extension...")
    warning = nil -- Doesn't apply
  end

  return warning, skiLen
end


-- ------

-- Extend the list entries to include the serial number or fingerprint of the certificate
local function extend_entries(  )
  -- os.execute()
  -- openssl command from bash 

end

-- ------

-- -- If ephemeral Diffie-Hellman handshake is available at the server,
-- --  output the cipher used (to see the available ciphers at the host see the
-- --  output of openssl ciphers) or the parameters of the handshake
-- --  (like the curve and point in the case of EC).
-- local function EDH_parameters(cert)

-- end

-- ------

-- #5. Output a warning if certificate validity is more recent than given date
-- DEFAULT given date: system time()
local function check_validity(cert, date)
  --Cert date format: Oct 30 13:14:00 2018 GMT

  local warning = false
  local lines = {}

    -- Stringify 'date' for latter comparison with Cert's Validity
  local dateStr = date_to_string(date)
    stdnse.debug1("DATE (to string): %s", dateStr)

    -- Certificate validity is not an extension. Cannot use get_extension()
  local notBefore = cert.validity.notBefore
  local notAfter = cert.validity.notAfter
    stdnse.debug1("Cert Validity; Not Before: %s", date_to_string(notBefore))
    stdnse.debug1("Cert Validity; Not After: %s", date_to_string(notAfter))

  lines[#lines + 1] = "Not valid before: " .. date_to_string(notBefore)
  lines[#lines + 1] = "Not valid after:  " .. date_to_string(notAfter)

  -- Checking 'notAfter' not more recent (not larger timestamp) than 'date'
  if ( ((date_to_string(notBefore)) > dateStr) or ((date_to_string(notAfter)) < dateStr)) then
    -- stdnse.debug1("Validity Warning: more RECENT than date (%s)", date)
    stdnse.debug1("Validity Alert: selected date is not in the valid period!")
    warning = true
  -- elseif (notAfter < dateStr) then stdnse.debug1("Out of validity") end
  end

  return warning, notBefore, notAfter
end

--_________________________________________________--
-----------------------------------------------------


-- Extracts the certificate's field of interest: Subject's Alternative Name 
--        (Basic + Enhanced Functionality)

local function output_check(cert, file)
  local lines = {}
    -- Enhanced functionality #1:
  local skiCheck -- boolean
  local skiLength
    -- Enhanced functionality #5:
  local date = os.time()
  local validityCheck -- boolean
  local notBefore
  local notAfter

  lines[#lines + 1] = "\n"
  -- lines[#lines + 1] = "Subject: " -- .. stringify_name(cert.subject)

  if cert.extensions then
    --    stdnse.debug1("output_check() - Checking EXTENSIONS..")
    for _, e in ipairs(cert.extensions) do

      if e.name == "X509v3 Subject Alternative Name" then

          stdnse.debug1("RAW ALTNAME: %s", e.value)

        --read 'name' values & process them
        local altnames = process_name(e.value) -- LIST of cert's AltNames
          for a in ipairs(altnames) do
            stdnse.debug1("Altnames processed: %s", altnames[a])
          end

        -- create table of distrusted domains
        local distT = distrusted_table(file)

        -- check for matches between the table and the cert's altnames
        --    altnames are already processed 
        local matches = is_distrusted(altnames, distT)

            stdnse.debug1("matches table : START ")
          for i in pairs(matches) do
            stdnse.debug1("MATCH: %s", matches[i])
            stdnse.debug1("#matches: %s", #matches)
          end
            stdnse.debug1("matches table : COMPLETE \n")

          local count = 1 -- Printing out numbered AltNames, no biggie
          for b in ipairs(altnames) do
            lines[#lines + 1] = "Subject Alternative Name #" .. count  .. ": " .. altnames[b]
            -- altnames are the processed strings, raw = e.value
            count = count + 1
          end

        if (#matches > 0) then
            --stdnse.debug1("*** MATCH! *** This server is potentially malicious!")
            print("*** MATCH! *** This server is potentially malicious! \n")

          lines[#lines + 1] = "TRUSTED?: NO"
          for d in ipairs(matches) do
            lines[#lines + 1] = "\t WARNING; Potentially dangerous DNS [" ..matches[d].. "]!"
          end
        else
          lines[#lines + 1] = "TRUSTED?: YES"
        end
        lines[#lines + 1] = "\n"
      end --SubjectAlternativeName
    end -- Extensions for()
  end   -- Extensions


  -- Enhanced Functionality #1
  skiCheck, skiLength = check_KeyID(cert)
  if (skiCheck ~= nil) then
    lines[#lines + 1] = "Subject Key Identifier Length: " .. skiLength

    if (skiCheck == true) then --not trusted, warning alert
      lines[#lines + 1] = "TRUSTED?: NO"
      lines[#lines + 1] = "\t WARNING; Potentially dangerous DNS!"
    else
      lines[#lines + 1] = "TRUSTED?: YES"
    end
    lines[#lines + 1] = "\n"
  else
    lines[#lines + 1] = "NO Subject Key ID Available"
    lines[#lines + 1] = "\n"
  end

  -- Enhanced Functionality #5
  validityCheck, notBefore, notAfter = check_validity(cert, date)
  lines[#lines + 1] = "Validity Period:"
  lines[#lines + 1] = "Not Before: " .. date_to_string(notBefore)
  lines[#lines + 1] = "Not After: " .. date_to_string(notAfter)

  if (validityCheck == true) then --not trusted, warning alert
    lines[#lines + 1] = "TRUSTED: NO"
    lines[#lines + 1] = "\t WARNING; selected date is not in the valid period!"
  else
    lines[#lines + 1] = "TRUSTED: YES"
  end
  lines[#lines + 1] = "\n"


  lines[#lines + 1] = "\n"
  return table.concat(lines, "\n")
end

  ------------------

--      ACTION      --

action = function(host, port)--, file)

  local file = stdnse.get_script_args("list")

  if (not file_exists(file)) then
    stdnse.debug1("Error reading file: NOT FOUND")
    return
  end

  host.targetname = tls.servername(host)
  local status, cert = sslcert.getCertificate(host, port)
  if ( not(status) ) then
    stdnse.debug1("getCertificate error: %s", cert or "unknown")
    return
  end
  stdnse.debug1("FILE & CERTIFICATE [ OK ] \n")

  return output_check(cert, file)
  --return stdnse.debug1("Program executing")
end

-------------------------------------
