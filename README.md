# distrusted_cert

## Base Functionality

Lua script for detecting servers using a x509 certificate with a SubjectAltName in a list of suspicious names or IPs.

Provide a _csv_ file with suspicious Domain Names. This file is specified using the option `--script-args 'list=<filename>'`.
If the option is not specified, the default name of the list file is _'list.csv'_.

**List entries:**
List entries are tuples of `<certainity;severity;name>` separated by newlines.
The entries denote the certainity of a given name being harmful and the severity of this risk 
(critical, high, warning, low) of the server.
Server names might contain wildcards.
Sample entries:
```
0.99;critical;*.badguys.com
0.5;warning;*.uncertainguys.com
0.75;high;badserver.acme.com
```


## Enhanced Functionality

1. Output a warning if the certificate includes a Subject Key Identifier longer than 20 bytes (40 hex digits). See https://securityboulevard.com/2018/02/examining-an-x509-covert-channel/.

2. Output a warning if the certificate validity is more recent than a given date.


## Output Samples

![Sample output: ncat server][output1]

![Sample output: test website][output2]

[output1]: /gfx/FINAL_at_entrega.png "Output img1"
[output2]: /gfx/FINAL_at_entrega2.png "Output img2"


## Usage
```
$ nmap -p <portnumber(443)> domain.url --script distrustedTLScert.nse --script-args list=<filename>.csv
```

**Debugging**
```
$ nmap -p <portnumber(443)> domain.url --script distrustedTLScert.nse --script-args 'list=<filename>' -v -d1
```