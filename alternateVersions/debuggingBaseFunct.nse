local datetime = require "datetime"
local nmap = require "nmap"
local shortport = require "shortport"
local sslcert = require "sslcert"
local stdnse = require "stdnse"
local string = require "string"
local table = require "table"
local tls = require "tls"
local unicode = require "unicode"


author = "J. Erustes & D. Ventas"
categories = {"default","discovery","vuln"} --triggered with -se option
--
-- Reference information:
--   ssl_cert Lua script
--   Lua 5.3 Documentation
--
-- USAGE:
--
-- nmap -p 4433 localhost --script=distrustedTLScerts.nse --script-args='list=list.csv'
--
-- Against external vulnerable server:
--
-- nmap —script=‘distrustedTLScerts.nse’ —script-args=“list=list.csv” -v -d1 -p 443 hack this.co.uk
--
-- ----------
--
--
-- SERVER & CERTIFICATE:
--
-- 1. Change openssl.cnf to Alternative Names and not rely on CA
--    Locate following sections:
--
--      distinguished_name  = req_distinguished_name
--      attributes    = req_attributes
--      x509_extensions = req_ext
--
--        [ req_distinguished_name ]
--            # fill with data to be used for the certificate
--
--        [ req_ext ]
--        subjectAltName = @alt_names

--        [ alt_names ]
--          DNS.1 = cyberattacks.com
--          DNS.2 = CAtechniques.com
--          DNS.3 = cyberAT.com
--          DNS.4 = CattackT.com
--
-- 2. Create RSA key & Certificate following the openssl.cnf configuration file
-- $ openssl req -x509 -newkey rsa:2048 -keyout key1.pem -out cert1.pem -days 365 -nodes -config openssl.cnf
--
-- 3. Launch Server with valid cert1 Certificate (signed and with AltNames)
-- $ openssl s_server -key key1.pem -cert cert1.pem -www
--
-- ------------
--
-- -> Generation of certificate and signature [DISENCOURAGED: Not totally valid]
--
--   Generate new key and certificate
-- $ openssl req -x509 -newkey rsa:2048 -keyout key1.pem -out cert1.pem -days 365 -nodes
--
--   Show certificate on screen in a legible way
-- $ openssl req -in cert1.pem -text -noout
--
--   Levantar HTTPS server on port 44300 with the certificate shown previously
-- $ openssl s_server -key key1.pem -cert cert1.pem -accept 44330 -www
--
--   Check it is correctly running, in browser
-- https://localhost:44330
--
--   Generate cerficate via command line with subjectAltName using existing key:
--          WARNING: this command creates REQUESTS with AltNames, not Certificates
--
-- $ openssl req -new -sha256 -key key1.pem -subj "/C=ES/ST=ES/O=Acme, Inc./CN=cyberattack.com"
--   -reqexts SAN -config <(cat /etc/ssl/openssl.cnf <(printf
--   "[SAN]\nsubjectAltName=DNS:cyberattack.com,DNS:www.cberattack.com,
--   DNS:sistemasciberataque.es,DNS:CAtechniques.ru")) -out cert2.pem
--
--
-- -> Alternative: Having a valid CA to sign        (Not tested)
-- $ openssl ca -cert cert1.pem -keyfile key1.pem -in cert2.pem -out servercrt.pem
--
--
--
-- -> Lauch of ssl Server:
--
-- $ cd ~/<working_directory>/certificates/
--    contains "key_.pem" key file, "cert_.pem" certificate file
--
-- $ openssl s_server -key key1.pem -cert cert1.pem -www
--
--
-- -> Check server is UP
--
-- $ netstat -atn | grep 4433
--
--
--
--  ----- DEBUGGING: check if file is openable -----
--
--    local f = io.open(file, "rb")
--    if (not f) then stdnse.debug1("output_check: ERROR WITH FILE")
--    else
--      stdnse.debug1("function_name: FILE [ OK ]")
--      f:close()
--    end
--
----- ----- ----- ----- ----- ----- -----
--
--
-- https://nmap.org/nsedoc/lib/nmap.html#get_ssl_certificate
--
-- https://www.lua.org/manual/5.3/
--
--



-- RULE SECTION --
portrule = function(host, port)
  return shortport.ssl(host, port) or sslcert.isPortSupported(port) or sslcert.getPrepareTLSWithoutReconnect(port)
end
  --triggered when relevant port is marked as 'open'



-- ACTION SECTION --
  --runs when the script rule triggers

-- See if the supicious-list file exists
function file_exists(file)

      -- file (param) != file (local variable)
      -- first if ... correct? Or is it reading something wrong..
      --

  local file = stdnse.get_script_args('list')

    if file == nil then
      file = "list.csv"
    end

    local f = io.open(file, "r")
    if f then f:close() end

    return f ~= nil
end


function retrieve_lines(file, altname)
  -- file = io.open(filename)
  --must be called before this function
  local exists;
  local lines = {}

  if not file_exists(file) then return {} end

  file:read()
  for line in io.lines(file) do
    -- lookup: alternative name/OU in the whole line
	  --! only for exact match of the string
    lines[#lines + 1] = line
	  exists = string.find(file, altname)
    if (exists ~= nil) then
      break
    end
  end
  --It can also just 'return exists' and then process later what to do with it
    -- no matches found
  if exists == nil then return {} end
  	--file exists
    -- ret integer: position returned by find()
  return exists
  --return file
	-- we could aslso return full string of blacklist containing altname
end

-- ssl_cert.nse
function stringify_name(name)
  local fields = {}
  local _, k, v
  if not name then
    return nil
  end
  for _, k in ipairs(NON_VERBOSE_FIELDS) do
    v = name[k]
    if v then
      fields[#fields + 1] = string.format("%s=%s", k, maybe_decode(v) or '')
    end
  end
  if nmap.verbosity() > 1 then
    for k, v in pairs(name) do
      -- Don't include a field twice.
      if not table_find(NON_VERBOSE_FIELDS, k) then
        if type(k) == "table" then
          k = table.concat(k, ".")
        end
        fields[#fields + 1] = string.format("%s=%s", k, maybe_decode(v) or '')
      end
    end
  end
  return table.concat(fields, "/")
end


local function lines_form(file)
  if (not file_exists(file)) then return {} end

  lines = {}
  for line in io.lines(file) do
    --write each line into one entry of the table
    lines[#lines + 1] = line
  end 
  --stdnse.debug1("lines_form() COMPLETE ")
  return lines 
end


local function split(splitStr, separation)
  local table1 = {}
  if separation == nil then
    separation = "%s"
  end

  local reg = string.format("([^%s]+)", separation)
  for mem in string.gmatch(splitStr, reg) do
    table.insert(table1, mem)
  end
  --stdnse.debug1("split() COMPLETE ")
  return table1
end


-- process altname to fit into desired format
  -- usually comes as 'DNS: altname'
local function process_name(raw)
  local line = split(raw, ": ")
  altn = line[2]

  return altn
end


-----------------------------

local function distrusted_table(file)
  -- distTable = {}

        -- TODO: next chunk not completely working, unused for now

  -- for line in io.lines(file) do
  --   l = line .. ";a"  -- TODO: Ñapa: match() ignores last word, I add dummy one
  --   local certainity, severity, name, tmp = l:match("%s*(.-);%s*(.-);%s*(.-);%s*(.-);")

  --     stdnse.debug1(certainity);

  --         --TODO: debugging
  --         stdnse.debug1("ÑAPA applied && match() executed ...")

  --     -- name comparison: support for wildcards
  --     -- gsub: global substitutions
  --   local cmpname = string.gsub(name, "*", ".*")
  --   distTable[#distTable + 1] = {certainity = certainity, severity = severity, name = name, cmpname = cmpname}
  -- end

  local fullTable = lines_form(file)
  local altnTable = {}

  -- Split fullTable (containing full lines)
  --    to leave only the AltNames
  for k in pairs(fullTable) do 
    local lineTable = split(fullTable[k], ";")
    altnTable[k] = lineTable[3]
  end

  -- print AltNames:
  -- for i in pairs(altnTable) do
  --   print(altnTable[i])
  -- end
  -- stdnse.debug1("AltNames table : COMPLETE ")
  return altnTable
end


local function is_distrusted(altname, distTable)
  local trusted = true
  local matches = {}
        --if (distTable == nil) then stdnse.debug1("is_distrusted(): #distTable: 0") end


              --TODO
              print("altname: ", altname, "\n")
              -- TODO: debugging
              stdnse.debug1("is_distrusted(): TRAVERSING distTable: \n")


  for k, t in pairs(distTable) do
  --if altname == string.match(altname, t.cmpname) then
  if altname == string.match(altname, t) then
        
              -- TODO: debugging
              stdnse.debug1("is_distrusted():MATCH FOUND! Saving to table")
              print(t)

      matches[#matches + 1] = t
        --if the altname is found in the blacklist, certificate is not trusted
      trusted = false
    end
  end
  -- table containing matches of the list. If ==nil, no matches found
              stdnse.debug1("\n is_distrusted() COMPLETE ")
  return matches --, trusted
end

--  TODO:
-- If there is more than one match. WORKS??
-- 


---------------------------


local function output_check(cert, file)
  --local o = stdnse.output_table()
  local lines = {}

  lines[#lines + 1] = "Subject: " -- .. stringify_name(cert.subject)
  --o.suject = ...
  if cert.extensions then
      --  stdnse.debug1("OUTPUT_CHECK() - Checking EXTENSIONS..")
    for _, e in ipairs(cert.extensions) do
      if e.name == "X509v3 Subject Alternative Name" then
        --read 'name' value & process it
        local altname = process_name(e.value)

                  --TODO
                  print("Altname processed: ", altname, "\n")


        local distT = distrusted_table(file)
          stdnse.debug1("CREATED DISTRUSTED TABLE  [ OK ]")


        local matches = is_distrusted(altname, distT)
          --TODO
            stdnse.debug1("matches table : START ")
            for i in pairs(matches) do
              print(matches[i])
              print(#matches)
            end
            stdnse.debug1("matches table : COMPLETE ")

        if (#matches > 0) then
        --if (is_distrusted(e.value, distT) ~= nil) then 
                -- table with AltNames is passed to function
          lines[#lines + 1] = "Subject Alternative Name: " .. e.value
          --o.Alt_name = ...

          stdnse.debug1("*** MATCH! *** This server is potentially malicious!")
        end
      end
    end
  end

  return table.concat(lines, "\n")
end

-- ------ ENHANCED FUNCTIONALITY -------

-- -- Output a warning if the certificate includes a Subject Key Identifier
-- --   longer than 20 bytes (40 hex digits).
-- local function output_warning(cert)
--   local lines = {}

--   for _, t in ipais(cert.extensions) do
--     if t.name == "Subject Key Identifier" then

--                                         -- TODO
--                                         -- e.name to access the SKI field?

--       lines[#lines + 1] = "SKI: " .. t.value
--       break
--     end
--   end
--   -- Only receive one SKI field, one entry on the table
--   if lines[0] == __ then

--     -- TODO
--   -- stdnse.tohex(lines[0] ) then   ¿¿?¿?¿?¿?¿?¿?¿
--   -- stdnse.fromhex()

--     -- io.write( )
--     stdnse.debug1("Security Error: %s", cert, "'s SKI too long.")
--     return
--   end
--   return --all ok
-- end

-- ------

-- -- If ephemeral Diffie-Hellman handshake is available at the server,
-- --  output the cipher used (to see the available ciphers at the host see the
-- --  output of openssl ciphers) or the parameters of the handshake
-- --  (like the curve and point in the case of EC).
-- local function EDH_parameters(cert)


-- end

-- ------

-- -- Output a warning if certificate validity is more recent than given date
-- local function check_validity(cert, date)
-- local lines = {}

--   lines[#lines + 1] = "Not valid before: " ..
--     date_to_string(cert.validity.notBefore)
--   lines[#lines + 1] = "Not valid after:  " ..
--     date_to_string(cert.validity.notAfter)

--   local notBefore = cert.validity.notBefore -- lines[1]
--   -- check
--   if (date < notBefore) then
--     stdnse.verbose1("Validity Warning: more RECENT than given time") --%s, date
--     return
--   end
-- end

-------------------------------------

-- -- tests the functions above
-- local file = 'test.lua'
-- local lines = retrieve_lines(file)

--      ACTION

action = function(host, port)--, file)
      -- TODO: stop using this hardwritten path and pass it by parameter
  --local file = '~/Dropbox/·UC3M/·Master Cuatri I/Cyber Attack Techniques/LAB/lab1/list.csv'
  local file = stdnse.get_script_args("list")

  if (not file_exists(file)) then
    stdnse.debug1("Error reading file: NOT FOUND")
    return
  end

  host.targetname = tls.servername(host)
  local status, cert = sslcert.getCertificate(host, port)
  if ( not(status) ) then
    stdnse.debug1("getCertificate error: %s", cert or "unknown")
    return
  end

  stdnse.debug1("FILE & CERTIFICATE [ OK ] \n")

  return output_check(cert, file)
  --return stdnse.debug1("Program executing")
end

-------------------------------------
